extern crate proc_macro;
use proc_macro::TokenTree;
use proc_macro::TokenStream;

/// Blocks compilation of the project if the crate's `version` exceeds the version passed to the macro.
/// 
/// # Usage
/// `todo_before!(version: &str, comments: &str)`
/// 
/// # Examples
/// ```
/// todo_before!("1.0.0", "Use Option<T> instead of panicking when an error occurs");
/// ```
#[proc_macro]
pub fn todo_before(input: TokenStream) -> TokenStream {

    // Pray that the first literal is the version, second is the comments
    let mut args: Vec<std::string::String> = vec![];
    for token in input {
        match token {
            // Add the literal to the `args` Vector after removing the quotation marks
            TokenTree::Literal(value) => {args.push(value.to_string().replace("\"", ""))},
            _ => {} // Ignore anything that isn't a literal
        }
    };

    // Error if we've extracted more than 2 arguments
    if args.len() != 2 {
        return "compile_error!(\"Malformed todo_before! invokation\");".parse().unwrap();
    }

    // Used for version parsing
    use semver::Version;

    // Check if the current version is lower than the trip version
    if Version::parse(env!("CARGO_PKG_VERSION")) > Version::parse(format!("{}", args[0]).as_str()) {
        // Throw a compiler error
        return format!("compile_error!(\"Fix required! Max version before fix: {}, Current Version: {}.\nComments: {}\");", args[0], env!("CARGO_PKG_VERSION"), args[1]).parse().unwrap()
    } else {
        // Do nothing
        return "".parse().unwrap()
    }
}

